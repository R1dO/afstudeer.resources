
.. Define common substitution directives.

.. |EOF| replace:: :term:`EOF`
.. |FY-2C| replace:: :term:`FY-2C`
.. |LST| replace:: :term:`LST`
.. |(M)SSA| replace:: :term:`(M)SSA`
.. |SSA| replace:: :term:`SSA`
.. |HANTS| replace:: :term:`HANTS`
