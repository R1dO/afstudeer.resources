#!/bin/bash
# Create archive of this ``_resources`` submodule.
# Contains pdf versions of bibtex entries.
#
# Separate download to allow disabling sharing those documents. In case this is
# not allowed, and to prevent too many binary blobs entering the repository.
#
# Since it is used in a standalone (submodule) repository which can be included
# anywhere, fixed repository paths are no longer possible.
#
# Be careful when changing script- ENVIRONMENTAL variables.

# Where am I?
MODULE_DIR=$(dirname $(readlink -f $0))

# Error and warning values
# ------------------------
E_NOINPUT=74  # C/C++ style exit code for missing file.
E_NOPERM=77   # C/C++ style exit code for permission denied.

####################################
###        User variables        ###
####################################
# Assume user has a separate dir to store dataset archives.
archive_folder=~/datasets/archives
archive_base_name="_resources_thesis_SSA"
resources_folder=${MODULE_DIR}

source_folders="books etc papers .timestamp*.cloud"

# Allow archive overwriting
# (Activate only when required due to changed source contents).
#OVERWRITE_ARCHIVES=true  # true or false

##############
#### MAIN ####
##############

echo -e "Archiving contents of:\n  ${resources_folder}"

if [ ! -d "${archive_folder}" ]
then
   echo -e "Directory missing:\n  ${archive_folder}"
   exit ${E_NOINPUT}
fi

archive_base_path=$(readlink -f ${archive_folder})/${archive_base_name}.tar.gz
if [ ! "$OVERWRITE_ARCHIVES" == true ] && [ -f "${archive_base_path}" ]
then
   echo -e "\nWe are not allowed to overwrite:\n  ${archive_base_path}"
   echo "Check permissions and/or force overwriting by setting ``OVERWRITE_ARCHIVES`` to 'true'."
else
   echo -e "Creating archive:\n ${archive_base_path}"

   # Update time-stamp inside archive (reStructuredText substitution format)
   archive_time_stamp=$(date -u)
   echo ".. |${archive_base_name}.cloud| replace:: $archive_time_stamp" > \
      ".timestamp-${archive_base_name}_archive.cloud"

   # Create archive.
   tar -czpf ${archive_base_path} ${source_folders}

fi

echo -e "\nTo check if it is needed to create a new archive, run:
   find ${source_folders} -newercc .timestamp-${archive_base_name}_archive
   .cloud"
