##########
Change Log
##########
.. contents:: :depth: 2

[Unreleased][unreleased]
========================
Added
-----

Changed
-------

Depreciated
-----------

Removed
-------

Fixed
-----


0.0.6_ - 2016-01-29
===================
Added
-----
* Svg images (Moved from original repository and converted from .pdf).
* Sphinx substitution directive for in-text glossary items.
* Added scripts to manage private archive(s) creation/extraction.
* Use version numbers from thesis repo.
* Bibtex entries
* Rules to ignore noise

Fixed
-----
* Non-breaking space unicode characters will upset sphinx-latex builder.


0.0.1_ - 2015-08-26
===================
Initial version after split from the afstudeer_ parent repository.

.. Hyperlinks
.. _afstudeer: https://bitbucket.org/R1dO/afstudeer
.. _0.0.1: https://bitbucket.org/R1dO/afstudeer.resources/commits/tag/split-from-parent
.. _0.0.6: https://bitbucket.org/R1dO/afstudeer.resources/branches/compare/0.0.6%0Dsplit-from-parent
