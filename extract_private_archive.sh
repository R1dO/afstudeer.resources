#!/bin/bash
# Extract private contents of this ``_resources`` submodule.
# Contains pdf versions of bibtex entries.
#
# Separate download to allow disabling sharing those documents. In case this is
# not allowed, and to prevent too many binary blobs entering the repository.
#
# Since it is used in a standalone (submodule) repository which can be included
# anywhere, fixed repository paths are no longer possible.

# Where am I?
MODULE_DIR=$(dirname $(readlink -f $0))

E_NOINPUT=74  # C/C++ style exit code for missing file.

####################################
###        User variables        ###
####################################
# Assume user has a separate dir to store dataset archives.
archive_folder=~/datasets/archives
archive_base_name="_resources_thesis_SSA"
resources_folder=${MODULE_DIR}

##############
#### MAIN ####
##############
echo -e "Extracting to:\n  ${resources_folder}"

if [ ! -d "${archive_folder}" ]
then
   echo -e "Directory missing:\n  ${archive_folder}"
   exit ${E_NOINPUT}
fi

archive_base_path=$(readlink -f ${archive_folder}/${archive_base_name}.tar.gz)
echo -e "Using the archive:\n  ${archive_base_path}"
if [ ! -f ${archive_base_path} ]
then
   echo -e "\nCannot find archive file:\n  ${archive_base_path}"
   echo "Please make sure it exists and caller script uses correct ``archive`` location."
   exit ${E_NOINPUT}
else
   echo "Extracting archive(s)."
   tar -xzpf ${archive_base_path} --keep-newer-files -C ${resources_folder}
   echo "Done"

   archive_time_stamp=$(date -u)
   # Update time-stamp inside archive (reStructuredText substitution format)
   echo ".. |${archive_base_name}.local| replace:: $archive_time_stamp" > \
      ".timestamp-${archive_base_name}_archive.local"
fi

echo -e "
NOTE:
If the bibtex database conflicts with the repository one. Please revert the \
changes.
It is safe to assume the latter one is more up-to date than the one just \
created from the downloaded archive."
