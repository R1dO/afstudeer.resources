================
Thesis resources
================
Contains data shared between different submodules (mostly reports) and
the parent repository.
This data must be available for each repository in order to keep them 
consistent for those cases that they are stand-alone.

The ``images`` folder serves as a linkfarm between the data of the base
repository afstudeer_ and the different folders/submodules that incorporate
this folder.

The ``*.sh`` scripts are there to provide easy access to non-repository data.

If extra folders are needed, try to adhere to the folder structure of 
afstudeer_.

.. Hyperlinks
.. _afstudeer: https://bitbucket.org/R1dO/afstudeer
